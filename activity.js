
// Using insertOne()
db.hotelRooms.insertOne(
    {
    "name": "single",
    "accomodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities.",
    "rooms_available": 10,
    "is_available": false
    }
)


// Using insertMany()
db.hotelRooms.insertMany([
    {
    "name": "double",
    "accomodates": 3,
    "price": 2000,
    "description": "A room fit for a small family going on a vacation.",
    "rooms_available": 5,
    "is_available": false
    },
    {
    "name": "queen",
    "accomodates": 4,
    "price": 4000,
    "description": "A room with a queen sized bed perfect for a simple getaway.",
    "rooms_available": 15,
    "is_available": false
    },
])


// finding a specific room
db.hotelRooms.find({"name": "double"})


// updating the queen room
db.hotelRooms.updateOne(
    {"name": "queen"},
    {$set: {"rooms_available": 0}}
)

db.hotelRooms.find({"name": "queen"})


// deleting 0 rooms available
db.hotelRooms.deleteMany({"rooms_available": 0})

db.hotelRooms.find()